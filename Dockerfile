ARG BRANCH_NAME=alpine

FROM registry.gitlab.com/a4sex/images/node:${BRANCH_NAME}

WORKDIR /semantic-release

COPY package.json .releaserc.cjs .

RUN yarn global add semantic-release @semantic-release/git @semantic-release/gitlab
