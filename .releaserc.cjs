module.exports = {
  plugins: [
      '@semantic-release/commit-analyzer',
      ['@semantic-release/npm', {
          'npmPublish': false
      }],
      // '@semantic-release/gitlab',
  ],
}